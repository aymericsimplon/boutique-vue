import HomeView from "@/views/HomeView.vue";
import ListFruits from "@/components/ListFruits.vue";
import { createRouter, createWebHistory } from "vue-router";

export const router  = createRouter({
  history: createWebHistory(),
  // linkActiveClass: 'active',
  routes: [
      {path: '/',
      component: HomeView
    },

      {
        path: '/listfruits',
        component: ListFruits
      },
  
  ]
});
export default router